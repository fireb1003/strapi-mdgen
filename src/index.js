import ApolloClient, { gql } from "apollo-boost";
import { GET_ARTICLES } from "./graphql/articles";
import fetch from "node-fetch";
import fse from "fs-extra";

require('dotenv').config();

const client = new ApolloClient({
  uri: process.env.STRAPI_GRAPHQL,
  fetch: fetch,
  onError: ({ networkError, graphQLErrors }) => {
    console.log("graphQLErrors", graphQLErrors);
    console.log("networkError", networkError);
  }
});

export async function getArticles() {
  let { data } = await client.query({
    query: GET_ARTICLES
  });
  return data;
}

function mdFileBody({
  title,
  cover,
  author,
  createdAt,
  type,
  description,
  layout,
  content
}) {
  return `+++
title= "${title}"
cover = "${cover}"
author = "${author}"
date = ${createdAt}
${type ? `type= "${type}"` : ""}
${layout ? `layout= "${layout}"` : ""}
description = """
${description ? description : ""}
"""
+++
${content}
`;
}

(async () => {
  try {
    let { articles } = await getArticles();
    console.log(articles);
    let fs_posts = articles.map(article => ({
      title: article.title,
      type: "post",
      body: mdFileBody({
        ...article,
        cover:
          article.extras && article.extras.cover ? article.extras.cover : "",
        author: "مؤمن"
      })
    }));

    await Promise.all(
      fs_posts.map(async post => {
        // first make post title safe to be a file
        // windows
        // fse.writeFileSync(`posts\\${post.title}.md`, post.body);
        fse.writeFileSync(`posts/${post.title}.md`, post.body);
      })
    );
  } catch (e) {
    // Deal with the fact the chain failed
    console.error(e);
  }
})();
