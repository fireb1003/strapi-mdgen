import { gql } from "apollo-boost";

export const GET_ARTICLES = gql`
  query getArticles($where: JSON) {
    articles(where: $where) {
      id
      title
      content
      description
      extras
      rawcontent
      createdAt
      updatedAt
      published
    }
  }
`;

export const GET_ARTICLE = gql`
  query getArticle($id: ID!) {
    article(id: $id) {
      id
      title
      content
      description
      extras
      rawcontent
      createdAt
      updatedAt
      published
    }
  }
`;
